package com.ms.sam.shared_preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    EditText et_name,et_contact,et_address;
    Button save_button;

    SharedPreferences sharedPreferences;
    String file_name="Data";

    SharedPreferences.Editor editor;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        et_address=findViewById(R.id.addressID);
        et_contact=findViewById(R.id.contactID);
        et_name=findViewById(R.id.nameID);

        save_button=findViewById(R.id.saveID);



        sharedPreferences=getSharedPreferences(file_name,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();

//        String name=sharedPreferences.getString("name",null);
//        String contact=sharedPreferences.getString("contact",null);
//        String address=sharedPreferences.getString("address",null);

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=et_name.getText().toString();
                String contact=et_contact.getText().toString();
                String address=et_address.getText().toString();


                editor.putString("name",name);
                editor.putString("address",address);
                editor.putString("contact",contact);

                editor.commit();
                Toast.makeText(SecondActivity.this, "saved data", Toast.LENGTH_SHORT).show();

                et_contact.setText("");
                et_address.setText("");
                et_name.setText("");

            }
        });


    }
}
