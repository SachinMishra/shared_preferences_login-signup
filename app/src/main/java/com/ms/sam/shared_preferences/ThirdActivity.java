package com.ms.sam.shared_preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    Button logout_button;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String file_name="Data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        logout_button=findViewById(R.id.logoutID);

        preferences=getSharedPreferences(file_name,Context.MODE_PRIVATE);
        editor=preferences.edit();

        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                editor.clear();
                editor.commit();

                Toast.makeText(ThirdActivity.this, "Log out", Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(ThirdActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });

    }
}
