package com.ms.sam.shared_preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {

    EditText et_name,et_contact,et_address;
    Button login_button,signup_button;

    SharedPreferences sharedPreferences;
    String file_name="Data";

    SharedPreferences.Editor editor;
    String status,status1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_address=findViewById(R.id.addressID);
        et_contact=findViewById(R.id.contactID);
        et_name=findViewById(R.id.nameID);
        signup_button=findViewById(R.id.signupID);

        login_button=findViewById(R.id.loginID);

        sharedPreferences=getSharedPreferences(file_name,Context.MODE_PRIVATE);

        editor=sharedPreferences.edit();




            status1=sharedPreferences.getString("status",null);

            if (status1!=null)
            {





              //  Toast.makeText(this, ""+status1, Toast.LENGTH_SHORT).show();


                Intent intent1 = new Intent(MainActivity.this, ThirdActivity.class);

                startActivity(intent1);


            }





        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name1=et_name.getText().toString();
                String contact1=et_contact.getText().toString();

                String name=sharedPreferences.getString("name",null);
                String contact=sharedPreferences.getString("contact",null);

                if (name1.equals(name)&&contact1.equals(contact))
               {
                    status="1";
                    editor.putString("status",status);
                    editor.commit();


                    status1=sharedPreferences.getString("status",null);

                   // Toast.makeText(MainActivity.this, ""+status1, Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(MainActivity.this,ThirdActivity.class);

                    startActivity(intent);

                }
                else {
                    Toast.makeText(MainActivity.this, "invalid user", Toast.LENGTH_SHORT).show();
                }




            }
        });

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                Intent intent=new Intent(MainActivity.this,SecondActivity.class);
                startActivity(intent);

            }
        });



    }
}
